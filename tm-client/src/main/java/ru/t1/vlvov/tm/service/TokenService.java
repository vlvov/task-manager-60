package ru.t1.vlvov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.ITokenService;

@Service
@Getter
@Setter
public final class TokenService implements ITokenService {

    @NotNull
    private String token;

}
