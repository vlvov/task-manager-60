package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.*;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.exception.AbstractException;
import ru.t1.vlvov.tm.listener.AbstractListener;
import ru.t1.vlvov.tm.util.SystemUtil;
import ru.t1.vlvov.tm.util.TerminalUtil;
import java.io.File;
import java.io.FileWriter;

@Getter
@Component
@ComponentScan("ru.t1.vlvov.tm")
public final class  Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.vlvov.tm.command";

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Autowired
    private AbstractListener [] abstractListeners;

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        final long pid = SystemUtil.getPID();
        File file = new File(fileName);
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(Long.toString(pid));
        myWriter.close();
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        fileScanner.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        fileScanner.stop();
    }

    public void run(@NotNull final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

    private boolean processArguments(@Nullable final String args[]) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        if (argument == null) return false;
        processArgument(argument);
        return true;
    }

    private void processArgument(@NotNull final String argument) {
        for (AbstractListener listener: abstractListeners) {
            if (listener.getArgument() == argument) {
                publisher.publishEvent(new ConsoleEvent(listener.getName()));
            }
        }
    }

    private void processCommands() {
        String command;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("OK");
            } catch (AbstractException e) {
                loggerService.error(e);
                System.err.println("FAIL");
            }
        }
    }

}
