package ru.t1.vlvov.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(TaskDTO task) {
        super(task);
    }

}
