package ru.t1.vlvov.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldNotFoundException {

    public UserIdEmptyException() {
        super("Error! User Id is empty...");
    }

}