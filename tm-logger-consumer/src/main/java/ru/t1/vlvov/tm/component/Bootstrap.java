package ru.t1.vlvov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.service.ReceiverService;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    public void run() {
        receiverService.receive();
    }

}
