package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.vlvov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.vlvov.tm.api.repository.model.IUserRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.vlvov.tm.repository.dto.UserDtoRepository;
import ru.t1.vlvov.tm.repository.model.UserRepository;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.vlvov.tm.constant.ProjectTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private IProjectDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN1);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(USER1.getId());
            repository.removeById(USER2.getId());
            repository.removeById(ADMIN1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            projectRepository.add(USER1_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1.getId(), projectRepository.findOneById(USER1_PROJECT1.getId()).getUserId());
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            projectRepository.clear(USER1.getId());
            Assert.assertTrue(projectRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findOneById(USER2_PROJECT1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (ProjectDTO project : PROJECT_LIST) {
                projectRepository.add(project);
            }
            Assert.assertFalse(projectRepository.findAll(USER1.getId()).isEmpty());
            projectRepository.clear(USER1.getId());
            System.out.println(projectRepository.findAll(USER1.getId()));
            Assert.assertTrue(projectRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertFalse(projectRepository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
            Assert.assertNull(projectRepository.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            projectRepository.removeById(USER1.getId(), USER1_PROJECT1.getId());
            Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findOneById(USER2_PROJECT1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            projectRepository.removeById(USER1.getId(), USER1_PROJECT1.getId());
            Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findOneById(USER2_PROJECT1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertNull(projectRepository.findOneById(USER2_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdOrdered() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (ProjectDTO project : PROJECT_LIST) {
                projectRepository.add(project);
            }
            System.out.println(projectRepository.findAll(USER1.getId(), Sort.BY_STATUS));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
