package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.dto.IUserDtoService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.dto.UserDtoService;
import ru.t1.vlvov.tm.service.model.UserService;
import ru.t1.vlvov.tm.util.HashUtil;

import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userServiceDTO = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Before
    public void init() {
        if (userService.isLoginExist(testLogin)) userService.removeByLogin(testLogin);
        if (userService.isEmailExist(testEmail)) {
            while (userService.findByEmail(testEmail) != null)
                userService.removeById(userService.findByEmail(testEmail).getId());
        }
    }

    @After
    public void tearDown() {
        if (userService.isLoginExist(USER1.getLogin())) userService.removeById(USER1.getId());
        if (userService.isLoginExist(USER2.getLogin())) userService.removeById(USER2.getId());
        if (userService.isLoginExist(ADMIN1.getLogin())) userService.removeById(ADMIN1.getId());
        if (userService.isLoginExist(testLogin)) userService.removeByLogin(testLogin);
    }

    @Test
    public void add() {
        userServiceDTO.add(USER1);
        Assert.assertEquals(USER1.getId(), userServiceDTO.findOneById(USER1.getId()).getId());
    }

    @Test
    public void addList() {
        for (final UserDTO user : USER_LIST) userServiceDTO.add(user);
        Assert.assertFalse(userServiceDTO.findAll().isEmpty());
    }

    @Test
    public void set() {
        for (final UserDTO user : USER_LIST) userServiceDTO.add(user);
        userService.clear();
        userServiceDTO.set(USER_LIST2);
        Assert.assertNull(userServiceDTO.findOneById(USER_LIST.get(0).getId()));
    }

    @Test
    public void clear() {
        for (final UserDTO user : USER_LIST) userServiceDTO.add(user);
        Assert.assertFalse(userServiceDTO.findAll().isEmpty());
        userService.clear();
        Assert.assertTrue(userServiceDTO.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        for (final UserDTO user : USER_LIST) userServiceDTO.add(user);
        Assert.assertFalse(userServiceDTO.findAll().isEmpty());
    }

    @Test
    public void findOneById() {
        for (final UserDTO user : USER_LIST) userServiceDTO.add(user);
        Assert.assertEquals(USER1.getId(), userServiceDTO.findOneById(USER1.getId()).getId());
    }

    @Test
    public void remove() {
        userServiceDTO.add(USER1);
        Assert.assertFalse(userServiceDTO.findAll().isEmpty());
        userServiceDTO.remove(USER1);
        Assert.assertNull(userServiceDTO.findOneById(USER1.getId()));
    }

    @Test
    public void removeById() {
        userServiceDTO.add(USER1);
        Assert.assertFalse(userServiceDTO.findAll().isEmpty());
        userServiceDTO.removeById(USER1.getId());
        Assert.assertNull(userServiceDTO.findOneById(USER1.getId()));
    }

    @Test
    public void existsById() {
        userServiceDTO.add(USER1);
        Assert.assertTrue(userServiceDTO.existsById(USER1.getId()));
        Assert.assertFalse(userServiceDTO.existsById(USER2.getId()));
    }

    @Test
    public void createLoginPassword() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword);
        Assert.assertTrue(userServiceDTO.existsById(user.getId()));
        Assert.assertEquals(testLogin, user.getLogin());
        Assert.assertEquals(HashUtil.salt(testPassword, new PropertyService()), user.getPasswordHash());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void createLoginPasswordEmail() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword, testEmail);
        Assert.assertTrue(userServiceDTO.existsById(user.getId()));
        Assert.assertEquals(testLogin, user.getLogin());
        Assert.assertEquals(HashUtil.salt(testPassword, new PropertyService()), user.getPasswordHash());
        Assert.assertEquals(testEmail, user.getEmail());
    }

    @Test
    public void createLoginPasswordRole() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword, Role.USUAL);
        Assert.assertTrue(userServiceDTO.existsById(user.getId()));
        Assert.assertEquals(testLogin, user.getLogin());
        Assert.assertEquals(HashUtil.salt(testPassword, new PropertyService()), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void findByLogin() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword, testEmail);
        Assert.assertEquals(user.getId(), userServiceDTO.findByLogin(user.getLogin()).getId());
    }

    @Test
    public void findByEmail() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword, testEmail);
        Assert.assertEquals(user.getId(), userServiceDTO.findByEmail(user.getEmail()).getId());
    }

    @Test
    public void isLoginExist() {
        userServiceDTO.add(USER1);
        Assert.assertTrue(userServiceDTO.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userServiceDTO.isLoginExist("NOT_EXIST_LOGIN"));
    }

    @Test
    public void isEmailExist() {
        userServiceDTO.add(USER1);
        Assert.assertTrue(userServiceDTO.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userServiceDTO.isLoginExist("NOT_EXIST_LOGIN"));
    }

    @Test
    public void removeByLogin() {
        userServiceDTO.add(USER1);
        Assert.assertFalse(userServiceDTO.findAll().isEmpty());
        userServiceDTO.removeByLogin(USER1.getLogin());
        Assert.assertNull(userServiceDTO.findByLogin(USER1.getLogin()));
    }

    @Test
    public void setPassword() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword);
        userServiceDTO.setPassword(user.getId(), "new_test_password");
        Assert.assertEquals(HashUtil.salt("new_test_password", new PropertyService()), userServiceDTO.findOneById(user.getId()).getPasswordHash());

    }

    @Test
    public void updateUser() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword);
        userServiceDTO.updateUser(user.getId(), "Ivan", "Ivanov", "Petrovich");
        Assert.assertEquals("Ivan", userServiceDTO.findOneById(user.getId()).getFirstName());
        Assert.assertEquals("Ivanov", userServiceDTO.findOneById(user.getId()).getLastName());
        Assert.assertEquals("Petrovich", userServiceDTO.findOneById(user.getId()).getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword);
        Assert.assertFalse(userServiceDTO.findOneById(user.getId()).getLocked());
        userServiceDTO.lockUserByLogin(user.getLogin());
        Assert.assertTrue(userServiceDTO.findOneById(user.getId()).getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull UserDTO user = userServiceDTO.create(testLogin, testPassword);
        Assert.assertFalse(userServiceDTO.findOneById(user.getId()).getLocked());
        userServiceDTO.lockUserByLogin(user.getLogin());
        Assert.assertTrue(userServiceDTO.findOneById(user.getId()).getLocked());
        userServiceDTO.unlockUserByLogin(user.getLogin());
        Assert.assertFalse(userServiceDTO.findOneById(user.getId()).getLocked());
    }

}
