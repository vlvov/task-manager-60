package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.dto.ISessionDtoService;
import ru.t1.vlvov.tm.api.service.dto.IUserDtoService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.dto.SessionDtoService;
import ru.t1.vlvov.tm.service.dto.UserDtoService;
import ru.t1.vlvov.tm.service.model.UserService;

import static ru.t1.vlvov.tm.constant.SessionTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionDtoService sessionService = new SessionDtoService(connectionService);

    @NotNull
    private final IUserDtoService userServiceDTO = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Before
    public void init() {
        userServiceDTO.add(USER1);
        userServiceDTO.add(USER2);
        userServiceDTO.add(ADMIN1);
    }

    @After
    public void tearDown() {
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN1.getId());
    }

    @Test
    public void add() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findOneById(USER1_SESSION1.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findOneById(USER1_SESSION1.getId()).getId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        for (final SessionDTO session : USER1_SESSION_LIST) sessionService.add(session);
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.clear(USER2.getId());
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.clear(USER1.getId());
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER2_SESSION1);
        sessionService.clear(USER1.getId());
        Assert.assertFalse(sessionService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        for (final SessionDTO session : SESSION_LIST) sessionService.add(session);
        Assert.assertFalse(sessionService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findOneById(USER1.getId(), USER1_SESSION1.getId()).getId());
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        sessionService.remove(USER1.getId(), USER1_SESSION1);
        Assert.assertNull(sessionService.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(sessionService.findOneById(USER2_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        sessionService.removeById(USER1.getId(), USER1_SESSION1.getId());
        Assert.assertNull(sessionService.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(sessionService.findOneById(USER2_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        Assert.assertTrue(sessionService.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2_SESSION1.getId()));
    }

}
