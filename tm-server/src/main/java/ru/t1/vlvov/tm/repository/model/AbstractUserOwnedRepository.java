package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

}
