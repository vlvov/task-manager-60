package ru.t1.vlvov.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.repository.model.IRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.IService;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Getter
    //@Autowired
    protected EntityManager entityManager;

    @NotNull
    private IRepository<M> repository;

    @Override
    @Transactional
    public void add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
    }

    @Override
    @Transactional
    public void update(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.update(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<M> collection) {
        repository.set(collection);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @Nullable
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    public M findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void remove(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        M model = findOneById(id);
        remove(model);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

}
