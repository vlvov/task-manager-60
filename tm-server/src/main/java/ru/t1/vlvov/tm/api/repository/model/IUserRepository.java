package ru.t1.vlvov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

}
