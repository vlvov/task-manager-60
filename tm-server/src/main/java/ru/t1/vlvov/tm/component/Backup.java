package ru.t1.vlvov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.service.IDomainService;
import ru.t1.vlvov.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class Backup {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public void save() {
        domainService.saveDataBackup();
    }

    public void load() {
        if (Files.exists(Paths.get(DomainService.FILE_BACKUP)))
            domainService.loadDataBackup();
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        load();
        es.scheduleAtFixedRate(this::save, 0, 3, TimeUnit.SECONDS);
    }

}
