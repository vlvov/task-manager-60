package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.repository.model.IRepository;
import ru.t1.vlvov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.IUserOwnedService;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    @Autowired
    @NotNull
    private IUserOwnedRepository<M> userOwnedRepository;

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        userOwnedRepository.add(userId, model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        userOwnedRepository.update(userId, model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        userOwnedRepository.clear(userId);
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return userOwnedRepository.findAll(userId);
    }

    @Override
    public @Nullable M findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return userOwnedRepository.findOneById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        userOwnedRepository.remove(userId, model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        M model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<M> findAll(@NotNull String userId, @NotNull Sort sort) {
        return null;
    }

}
