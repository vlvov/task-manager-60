package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>> extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    @NotNull
    @Autowired
    protected IUserOwnedDtoRepository<M> repositoryDTO;

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        repositoryDTO.add(userId, model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        repositoryDTO.update(userId, model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        repositoryDTO.clear(userId);
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return repositoryDTO.findAll(userId);
    }

    @Override
    public @Nullable M findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return repositoryDTO.findOneById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        repositoryDTO.remove(userId, model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        M model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        return repositoryDTO.findAll(userId, sort);
    }

}
