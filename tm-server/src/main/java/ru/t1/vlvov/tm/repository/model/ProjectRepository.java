package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.api.repository.model.IProjectRepository;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.Project;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public void clear() {
        @Nullable final List<Project> projects = findAll();
        if (projects != null && !projects.isEmpty())
            for (@NotNull final Project project : projects) entityManager.remove(project);
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project m";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final Project model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Project m WHERE user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @Nullable
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE user.id = :userId AND id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project model = findOneById(userId, id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<Project> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery = criteriaBuilder.createQuery(Project.class);
        Root<Project> project = criteriaQuery.from(Project.class);
        criteriaQuery.select(project)
                .where(criteriaBuilder.equal(project.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(project.get(Sort.getOrderByField(sort))));
        TypedQuery<Project> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

}
