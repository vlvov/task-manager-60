package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.api.repository.model.IUserRepository;
import ru.t1.vlvov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        List<User> users = findAll();
        if (users != null && !users.isEmpty())
            for (@NotNull final User user : users) entityManager.remove(user);
    }

    @Override
    @Nullable
    public List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @Nullable
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final User model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

}
