package ru.t1.vlvov.tm.configuration;

import com.mchange.v2.c3p0.DriverManagerDataSource;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.listener.EventListener;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.vlvov.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClass(propertyService.getDatabaseDriver());
        dataSource.setJdbcUrl(propertyService.getDatabaseUrl());
        dataSource.setUser(propertyService.getDatabaseUser());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory,
                                                         @NotNull final EventListener eventListener) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        initListener(entityManagerFactory.getObject(), eventListener);
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.vlvov.tm.dto.model", "ru.t1.vlvov.tm.model");
        @NotNull final Properties settings = new Properties();
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddl());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCash());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseConfigFilePath());
        settings.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        factoryBean.setJpaProperties(settings);
        return factoryBean;
    }

    private void initListener(@NotNull final EntityManagerFactory entityManagerFactory, @NotNull final EventListener eventListener) {
        @NotNull final SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registryListener = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registryListener.getEventListenerGroup(EventType.POST_INSERT).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_DELETE).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_UPDATE).appendListener(eventListener);
    }

}
